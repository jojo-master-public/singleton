using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Dioinecail.ServiceLocator
{
    [Serializable]
    public class ConfigByPlatform
    {
        public RuntimePlatform Platform;
        public BootstrapperConfig Config;
    }

    public class ServicesBootstrapper : MonoBehaviour
    {
        #region Fields
        // to make sure we bootstrap only once per game instance
        private static ServicesBootstrapper m_Instance;

        [SerializeField] private ConfigByPlatform[] m_Configs;
        [SerializeField] private BSConfigEntry[] m_SceneObjects;

        private bool m_IsDisposing = true;

        #endregion

        #region Unity Events

        private void Awake()
        {
            if (m_Instance != null)
            {
                m_IsDisposing = false;
                Destroy(gameObject);
                return;
            }
            else
            {
                m_Instance = this;
            }

            transform.SetParent(null);
            DontDestroyOnLoad(gameObject);
            Bootstrap();
        }

        private void OnDestroy()
        {
            if(m_IsDisposing)
                ServiceLocator.Dispose();
        }

        #endregion

        #region Core

        private void Bootstrap()
        {
            try
            {
                AddDefaultImplementations();
                AddCustomImplementations();
            }
            catch (Exception)
            {
                throw;
            }

            ServiceLocator.Init();
        }

        #endregion

        #region Utility

        private void AddDefaultImplementations()
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies().Reverse())
            {
                var types = assembly.GetTypes();

                foreach (var t in types)
                {
                    var defImplAttribute = t.GetCustomAttribute<DefaultImplementationAttribute>();

                    if (defImplAttribute != null)
                    {
                        ServiceLocator.Add(defImplAttribute.InterfaceType, t);
                    }
                }
            }
        }

        private void AddCustomImplementations()
        {
            var config = m_Configs.FirstOrDefault(c => c.Platform == Application.platform);

            if (config == null || config.Config.Entries.Count == 0)
                return;

            var interfaces = config.Config.Entries.Select(s => s.Interface).Distinct();
            var types = ByName(interfaces);

            foreach (var entry in config.Config.Entries)
            {
                var interfaceType = types.Where(x => x.FullName == entry.Interface).First();

                if (entry.ImplementationPrefab != null)
                {
                    var servicePrefab = Instantiate(entry.ImplementationPrefab, transform).GetComponent<IService>();

                    ServiceLocator.Add(interfaceType, servicePrefab);

                    continue;
                }

                var serviceType = types.Where(x => x.FullName == entry.Implementation).First();

                ServiceLocator.Add(interfaceType, serviceType);
            }

            foreach (var entry in m_SceneObjects)
            {
                var interfaceType = types.Where(x => x.FullName == entry.Interface).First();
                var serviceType = entry.ImplementationPrefab.GetComponent<IService>();

                ServiceLocator.Add(interfaceType, serviceType);
            }
        }

        private List<Type> ByName(IEnumerable<string> names)
        {
            List<Type> types = new List<Type>();

            foreach (var name in names)
            {
                foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies().Reverse())
                {
                    var tt = assembly.GetType(name);

                    if (tt != null)
                    {
                        types.AddRange(assembly.GetTypes());
                    }
                }
            }

            return types;
        }

        #endregion
    }
}