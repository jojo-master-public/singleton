using System;

namespace Dioinecail.ServiceLocator
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DefaultImplementationAttribute : Attribute
    {
        public readonly Type InterfaceType;



        public DefaultImplementationAttribute(Type interfaceType)
        {
            this.InterfaceType = interfaceType;
        }
    }
}