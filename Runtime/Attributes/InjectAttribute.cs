using System;

namespace Dioinecail.ServiceLocator
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class InjectAttribute : Attribute
    {

    }
}