namespace Dioinecail.ServiceLocator
{
    public interface IService
    {
        /// <summary> Initialize your dependencies here</summary>
        void InitDeps();
        /// <summary> Start using your dependencies and all code here </summary>
        void StartSystem();
    }
}