using System.Collections.Generic;
using UnityEngine;

namespace Dioinecail.ServiceLocator
{
    [System.Serializable]
    public class BSConfigEntry
    {
        public string Interface;
        public string Implementation;
        public GameObject ImplementationPrefab;
    }

    [CreateAssetMenu]
    public class BootstrapperConfig : ScriptableObject
    {
        public List<BSConfigEntry> Entries;
    }
}