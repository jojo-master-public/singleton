using System;
using System.Collections.Generic;
using System.Linq;

namespace Dioinecail.ServiceLocator
{
    public class ServiceLocator
    {
        #region Fields

        private static Dictionary<Type, object> m_Services = new Dictionary<Type, object>();

        public static Dictionary<Type, object> Services => m_Services;

        #endregion

        #region Public Methods

        public static T Get<T>() where T : class
        {
            var type = typeof(T);

            if (m_Services.ContainsKey(type))
            {
                return (T)m_Services[type];
            }
            else 
            {
                var foundServiceKey = m_Services.Keys.FirstOrDefault(s => s.GetType().IsAssignableFrom(type));

                if(foundServiceKey != null)
                {
                    return (T)m_Services[foundServiceKey];
                }

                var foundService = m_Services.Values.FirstOrDefault(s => s.GetType().IsAssignableFrom(type));

                return (T)(foundService ?? default(T));
            }
        }

        public static List<T> GetAll<T>()
        {
            var type = typeof(T);
            var foundServiceKey = m_Services.Keys.FirstOrDefault(s => s.GetType().IsAssignableFrom(type));

            if (foundServiceKey == null)
            {
                throw new Exception($"[ServiceLocator.GetAll] Type '{type.FullName}' not found in services");
            }

            var foundServices = m_Services.Values.Where(s => s.GetType().IsAssignableFrom(type)).ToList();
            var list = new List<T>();

            foreach (var s in foundServices)
            {
                list.Add((T)s);
            }

            return list;
        }

        public static void Add(Type interfaceType, Type implementationType, bool inject = false)
        {
            IService obj = (IService)Activator.CreateInstance(implementationType);

            Add(interfaceType, obj, inject);
        }

        public static void Add(Type interfaceType, IService implementation, bool inject = false)
        {
            if (!m_Services.ContainsKey(interfaceType))
            {
                m_Services.Add(interfaceType, implementation);

                if(inject)
                {
                    Inject(implementation);
                }
            }
        }

        public static void Remove(Type interfaceType)
        {
            if(m_Services.ContainsKey(interfaceType))
                m_Services.Remove(interfaceType);
        }

        #endregion

        #region Internal Methods

        internal static void Init()
        {
            foreach(var service in Services.Values)
            {
                Inject(service);
            }

            foreach (var service in Services.Values)
            {
                ((IService)service).InitDeps();
            }

            foreach (var service in Services.Values)
            {
                ((IService)service).StartSystem();
            }
        }

        internal static void Inject(object service)
        {
            var sType = service.GetType();
            var fields = sType.GetFields(System.Reflection.BindingFlags.Instance
                | System.Reflection.BindingFlags.Public
                | System.Reflection.BindingFlags.NonPublic)
                .Where(f => f.GetCustomAttributes(typeof(InjectAttribute), false).Length > 0);

            foreach (var field in fields)
            {
                var fieldType = field.FieldType;

                if (Services.TryGetValue(fieldType, out var foundService))
                {
                    field.SetValue(service, foundService);
                }
            }

            var properties = sType.GetProperties(System.Reflection.BindingFlags.Instance
                | System.Reflection.BindingFlags.Public
                | System.Reflection.BindingFlags.NonPublic)
                .Where(f => f.GetCustomAttributes(typeof(InjectAttribute), false).Length > 0);

            foreach (var property in properties)
            {
                var propertyType = property.PropertyType;

                if (Services.TryGetValue(propertyType, out var foundService))
                {
                    property.SetValue(service, foundService);
                }
            }
        }

        internal static void Dispose()
        {
            m_Services.Clear();
            m_Services = null;
        }

        #endregion
    }
}