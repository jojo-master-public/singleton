using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Dioinecail.ServiceLocator.Editors
{
    [CustomPropertyDrawer(typeof(BSConfigEntry))]
    public class BSConfigEntryPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var targetInterface = property.FindPropertyRelative(nameof(BSConfigEntry.Interface));
            var targetImplementation = property.FindPropertyRelative(nameof(BSConfigEntry.Implementation));
            var targetImplPrefab = property.FindPropertyRelative(nameof(BSConfigEntry.ImplementationPrefab));

            var interfaces = GetInterfaces();

            if (interfaces.Count == 0)
            {
                EditorGUI.LabelField(position, $"No {nameof(IService)}s have been created yet");
                return;
            }

            var implementations = GetImplementations(targetInterface.stringValue);

            EditorGUI.BeginProperty(position, label, property);

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            var width = position.width;
            var halfWidth = width / 2f;
            var height = 20f;

            var interfaceHeaderPosition = new Rect(position.position.x, position.position.y, halfWidth, height);
            var interfaceDropdownPosition = new Rect(position.position.x, position.position.y + height, halfWidth, height);

            var implementationHeaderPosition = new Rect(position.position.x + halfWidth, position.position.y, halfWidth, height);
            var implementationDropdownPosition = new Rect(position.position.x + halfWidth, position.position.y + height, halfWidth, height);

            var implenentationPrefabPosition = new Rect(position.position.x, position.position.y + height + height, width, height);

            if (DrawInterfaceProperty(interfaceHeaderPosition, interfaceDropdownPosition, targetInterface, interfaces))
                return;

            if (DrawImplementationProperty(implementationHeaderPosition, implementationDropdownPosition, targetImplementation, implementations))
                return;

            EditorGUI.ObjectField(implenentationPrefabPosition, targetImplPrefab);

            if (targetImplPrefab.objectReferenceValue != null)
            {
                var pos4 = new Rect(position.position.x, position.position.y + (height * 3), width, height);

                if (((GameObject)targetImplPrefab.objectReferenceValue).GetComponent<IService>() == null)
                    EditorGUI.HelpBox(pos4, "Target Object is not of ISystem type!", MessageType.Error);
            }

            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }

        private bool DrawInterfaceProperty(Rect headerPos, Rect dropDownPos, SerializedProperty targetInterface, List<string> interfaces)
        {
            EditorGUI.LabelField(headerPos, new GUIContent(nameof(BSConfigEntry.Interface)));

            var selectedIndexInterface = interfaces.IndexOf(targetInterface.stringValue);

            if (selectedIndexInterface < 0)
                selectedIndexInterface = 0;

            var newIndexInterface = EditorGUI.Popup(dropDownPos, selectedIndexInterface, interfaces.ToArray());

            if (newIndexInterface != selectedIndexInterface
                || !string.Equals(interfaces[selectedIndexInterface], targetInterface.stringValue))
            {
                var newValue = interfaces[newIndexInterface];
                targetInterface.stringValue = newValue;
                return true;
            }

            return false;
        }

        private bool DrawImplementationProperty(Rect headerPos, Rect dropDownPos, SerializedProperty targetImplementation, List<string> implementations)
        {
            EditorGUI.LabelField(headerPos, new GUIContent(nameof(BSConfigEntry.Implementation)));

            var selectedImplementation = implementations.IndexOf(targetImplementation.stringValue);

            if (selectedImplementation < 0)
                selectedImplementation = 0;

            var newImplementation = EditorGUI.Popup(dropDownPos, selectedImplementation, implementations.ToArray());

            if (newImplementation != selectedImplementation || !string.Equals(implementations[newImplementation], targetImplementation.stringValue))
            {
                var newValue = implementations[newImplementation];
                targetImplementation.stringValue = newValue;
                return true;
            }

            return false;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var targetImplPrefab = property.FindPropertyRelative("ImplementationPrefab");

            if (targetImplPrefab.objectReferenceValue != null)
            {
                if (((GameObject)targetImplPrefab.objectReferenceValue).GetComponent<IService>() == null)
                    return 80f;
            }

            return 60f;
        }

        List<string> GetInterfaces()
        {
            var list = new List<string>();

            var types = TypeCache.GetTypesDerivedFrom(typeof(IService));

            foreach (var t in types)
            {
                if (t.GetInterface(typeof(IService).Name) != null)
                {
                    list.Add(t.FullName);
                }
            }

            return list;
        }

        List<string> GetImplementations(string name)
        {
            var list = new List<string>();

            if (string.IsNullOrEmpty(name))
                return list;

            var types = TypeCache.GetTypesDerivedFrom(typeof(IService));

            foreach (var t in types)
            {
                if (!t.IsInterface && t.GetInterface(name) != null
                    || t.FullName == name)
                {
                    list.Add(t.FullName);
                }
            }

            return list;
        }
    }
}