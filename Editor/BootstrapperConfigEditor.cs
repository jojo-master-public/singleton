//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;
//using System.Reflection;

//namespace Project.Core
//{
//    [CustomEditor(typeof(BootstrapperConfig))]
//    public class BootstrapperConfigEditor : Editor
//    {
//        private SerializedProperty m_Entries;



//        private void OnEnable()
//        {
//            m_Entries = serializedObject.FindProperty("Entries");
//        }

//        public override void OnInspectorGUI()
//        {
//            DrawEntriesList();
//        }

//        private void DrawEntriesList()
//        {
//            EditorGUILayout.BeginHorizontal();

//            var arraySize = m_Entries.arraySize;

//            if (GUILayout.Button("Add"))
//            {
//                m_Entries.InsertArrayElementAtIndex(arraySize);
//                return;
//            }

//            if (GUILayout.Button("Remove"))
//            {
//                if (arraySize > 0)
//                {
//                    m_Entries.DeleteArrayElementAtIndex(arraySize - 1);

//                    return;
//                }
//            }

//            EditorGUILayout.EndHorizontal();

//            EditorGUILayout.Space(5);

//            EditorGUILayout.BeginHorizontal();
//            EditorGUILayout.LabelField("Interface:");
//            EditorGUILayout.LabelField("Implementation:");
//            EditorGUILayout.EndHorizontal();

//            var interfacesList = GetInterfaces();

//            for (int i = 0; i < arraySize; i++)
//            {
//                var prop = m_Entries.GetArrayElementAtIndex(i);
//                var m_Interface = prop.FindPropertyRelative("m_Interface");

//                EditorGUILayout.BeginHorizontal();

//                DrawInterface(prop, "m_Interface", interfacesList);
//                DrawImplementation(prop, "m_Implementation", m_Interface.stringValue);

//                EditorGUILayout.EndHorizontal();
//            }
//        }

//        private void DrawInterface(SerializedProperty prop, string name, List<string> interfacesList)
//        {
//            var m_Interface = prop.FindPropertyRelative(name);
//            var selectedIndex = interfacesList.IndexOf(m_Interface.stringValue);

//            if (selectedIndex < 0)
//                selectedIndex = 0;

//            var newIndex = EditorGUILayout.Popup(selectedIndex, interfacesList.ToArray());

//            if (newIndex != selectedIndex)
//            {
//                var newValue = interfacesList[newIndex];
//                m_Interface.stringValue = newValue;
//                serializedObject.ApplyModifiedProperties();
//            }
//        }

//        private void DrawImplementation(SerializedProperty prop, string name, string interfaceName)
//        {
//            var m_Implementation = prop.FindPropertyRelative(name);

//            var list = GetImplementations(interfaceName);
//            var selectedIndex = list.IndexOf(m_Implementation.stringValue);

//            if (selectedIndex < 0)
//                selectedIndex = 0;

//            var newIndex = EditorGUILayout.Popup(selectedIndex, list.ToArray());
//            var newValue = list[newIndex];

//            if (newIndex != selectedIndex || !string.Equals(m_Implementation.stringValue, newValue))
//            {
//                m_Implementation.stringValue = list[newIndex];
//                serializedObject.ApplyModifiedProperties();
//            }
//        }

//        private List<string> GetInterfaces()
//        {
//            var list = new List<string>();

//            var asmb = Assembly.GetAssembly(typeof(ISingletonObject));
//            var types = asmb.GetTypes();

//            foreach (var t in types)
//            {
//                if (t.IsInterface && t.GetInterface(typeof(ISingletonObject).Name) != null)
//                {
//                    list.Add(t.Name);
//                }
//            }

//            return list;
//        }

//        private List<string> GetImplementations(string name)
//        {
//            var list = new List<string>();

//            var asmb = Assembly.GetAssembly(typeof(ISingletonObject));
//            var types = asmb.GetTypes();

//            foreach (var t in types)
//            {
//                if (!t.IsInterface && t.GetInterface(name) != null)
//                {
//                    list.Add(t.Name);
//                }
//            }

//            return list;
//        }
//    }
//}