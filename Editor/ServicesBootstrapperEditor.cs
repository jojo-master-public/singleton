using UnityEditor;

namespace Dioinecail.ServiceLocator
{
    [CustomEditor(typeof(ServicesBootstrapper))]
    public class ServicesBootstrapperEditor : Editor
    {
        private ServicesBootstrapper m_Bootstrapper;



        private void OnEnable()
        {
            m_Bootstrapper = target as ServicesBootstrapper;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (m_Bootstrapper != null && ServiceLocator.Services != null)
            {
                foreach (var m in ServiceLocator.Services)
                {
                    var target = m.Value.GetType();

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField(m.Key.Name);
                    EditorGUILayout.LabelField(target.Name);

                    EditorGUILayout.EndHorizontal();
                }
            }
        }
    }
}