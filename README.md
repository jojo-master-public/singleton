# Service Locator

A pretty simple ServiceLocator package that doesn't rely on gameobjects to hold a service referense.

## Features

* A single gameobject needed to bootstrap the whole Services solution.
* Easy to use architecture.

## Getting Started

Create a ``BootstrapperConfig`` asset using right-click in Project panel.  
Inherit your singleton classes from ``IService`` interface.  
Add ``[Inject]`` attribute to dependencies of ``IService`` implementations.  
Add your implementations to the ``BootstrapperConfig`` asset.  
Add ``ServicesBootstrapper`` monobehaviour to any object in your first scene, make sure it has no parent.  
Call ``ServiceLocator.Get<T>()`` from anywhere to get a reference to a singleton object of type T, make sure that type T is added into ``BoostrapperConfig``  

## Contact

Developed by Andrew Dioinecail.  
  
Twitter: [CptCrabhands](https://twitter.com/cptcrabhands).  
  
email: andrewdionecail@gmail.com  
  
Feel free to ask questions!  